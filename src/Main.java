import com.sun.deploy.net.MessageHeader;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class Main {

    private static Scanner scanner = new Scanner(System.in);
    private static int count;
    private static int amount;
    private static ArrayList<Question> question = new ArrayList<>();
    private static int answerTrue = 0;
    private static int totalScore = 0;
    private static ArrayList<Integer> randomNums;

    public static void main(String[] args) {

        Main main = new Main();
        boolean start = true;
        while (start==true){
            main.help();
            System.out.println("|Введите номер команды|");
            int command = Integer.parseInt(scanner.nextLine());
        switch (command) {
            case 1:
                System.out.println("-Введите количество вопросов-");
                count = Integer.parseInt(scanner.nextLine());
                main.enterQuestion();
                break;
            case 2:
                System.out.println("-Тестирование-");
                System.out.println("*Введите количество вопросов для теста"+" (<= "+ question.size() + ")");
                amount = Integer.parseInt(scanner.nextLine());
                while (amount>question.size()){
                    System.out.println("*Введите корректное число вопросов"+" (<= "+ question.size() + ")");
                    amount = Integer.parseInt(scanner.nextLine());
                }
                randomNums = main.generateRandom(amount);
                main.testing();
                break;
            case 3:
                main.outputOfAnswers(randomNums);
                break;
            case 4:
                start = false;
                break;
        }
        }


    }

    public void help() {
        System.out.println("*Доступные команды");
        System.out.println("1. Добавить вопросы");
        System.out.println("2. Пройти тест");
        System.out.println("3. Вывести результаты");
        System.out.println("4. Закрыть программу");
    }

    public void enterQuestion(){
        for (int i=0; i<count; i++){
            System.out.println("*Введите вопрос");
            String ques = scanner.nextLine();
            ArrayList<String> answer = new ArrayList<>();
            System.out.println("*Введите количество возможных вариантов ответа");
            int countans = Integer.parseInt(scanner.nextLine());
            System.out.println("*Введите возможные варианты ответа");
            for (int j=0; j<countans; j++){
                answer.add(scanner.nextLine());
            }
            System.out.println("*Введите номер правильного ответа");
            int ans = Integer.parseInt(scanner.nextLine());
            System.out.println("*Введите стоимость вопроса");
            int score = Integer.parseInt(scanner.nextLine());
            Question question1 = new Question(ques, answer.get(ans-1), answer, false, score);
            question.add(question1);
        }
    }

    public void testing(){

        for (int i=0; i<amount; i++){
            System.out.println("•Вопрос " + (i+1)+ ": " + question.get(randomNums.get(i)-1).getQuestion());
            System.out.println("Варианты ответа:");
            for (int j=0; j<question.get(randomNums.get(i)-1).all.size(); j++){
                System.out.println((j+1) + ". " + question.get(randomNums.get(i)-1).all.get(j));
            }
            System.out.println("Выберете номер правильнго ответа");
            int answernew = Integer.parseInt(scanner.nextLine())-1;
            if (question.get(randomNums.get(i)-1).all.get(answernew).equals(question.get(randomNums.get(i)-1).answer)){
                answerTrue++;
                totalScore = totalScore + question.get(randomNums.get(i)-1).score;
                question.get(randomNums.get(i)-1).right=true;
            }
        }

    }

    public void outputOfAnswers(ArrayList<Integer> randomNums){

        for (int i=0; i<amount; i++){
            if (question.get(randomNums.get(i)-1).right){
                System.out.println("•Вопрос " + (i+1)+ ": " + question.get(randomNums.get(i)-1).getQuestion() + " ✓верно");
                System.out.println("Правильный ответ: "+ question.get(randomNums.get(i)-1).answer);
            } else {
                System.out.println("•Вопрос " + (i+1)+ ": " + question.get(randomNums.get(i)-1).getQuestion() + "  ✕неверно");
                System.out.println("Правильный ответ: "+ question.get(randomNums.get(i)-1).answer);
            }
        }
        System.out.println("-----------------------");
        System.out.println("(" + answerTrue + " верных ответов из " + amount + ")");
        System.out.println("Набранный балл: " + totalScore);
        System.out.println("-----------------------");
    }

    ArrayList<Integer> generateRandom(int amount){
        ArrayList<Integer> nums = new ArrayList<>();
        Random random = new Random();
        int num = question.size();
        for(int j = 0; j < num; j++) {
            nums.add(j+1);
        }
        ArrayList<Integer> randomNums = new ArrayList<>();
        for(int i = amount; i > 0; i--){
            int k = random.nextInt(num);
            randomNums.add(nums.get(k));
            nums.remove(k);
            nums.trimToSize();
            num--;
        }
        return randomNums;
    }




}


