import java.util.ArrayList;

public class Question {

    public String question;
    public String answer;
    public ArrayList<String> all;
    public boolean right;
    public int score;




    public Question(String question, String answer, ArrayList<String> all, boolean right, int score) {
        this.question = question;
        this.answer = answer;
        this.all = all;
        this.right = right;
        this.score = score;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public boolean isRight() {
        return right;
    }

    public void setRight(boolean right) {
        this.right = right;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public ArrayList<String> getAll() {
        return all;
    }

    public void setAll(ArrayList<String> all) {
        this.all = all;
    }

}
